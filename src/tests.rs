use crate::bot;
use mwbot::Bot;
use rfa_voting_history::Position;
use rfa_voting_history::Position::{Oppose, Support, Unknown};

async fn assert_vote(bot: &Bot, page: &str, username: &str, result: Position) {
    let page = bot
        .page(&format!("Wikipedia:Requests for adminship/{}", page))
        .unwrap();
    let vote = rfa_voting_history::parser::extract_vote(page, &[username])
        .await
        .unwrap();
    dbg!(&vote);
    let vote = vote.unwrap();
    assert_eq!(vote.position, result);
}

#[tokio::test]
async fn test_rfas() {
    let bot = bot().await;
    assert_vote(bot, "Pengo", "Espresso Addict", Support).await;
    assert_vote(bot, "Aranda56", "Kappa", Unknown).await;
    assert_vote(bot, "Aranda56", "Cryptic", Oppose).await;
    assert_vote(bot, "Acetic Acid", "Cryptic", Oppose).await;
    assert_vote(bot, "Kappa", "Kappa", Unknown).await;
    assert_vote(bot, "Kappa 2", "Kappa", Unknown).await;
    assert_vote(bot, "Kappa 2", "Fawcett5", Oppose).await;
    assert_vote(bot, "Sro23", "Cryptic", Unknown).await;
    assert_vote(bot, "Newyorkbrad", "SandyGeorgia", Support).await;
}
