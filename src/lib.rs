use mwbot::Page;
use serde::Serialize;

pub mod parser;
pub mod query;

#[derive(Debug, Serialize, Eq, PartialEq)]
pub enum Position {
    Support,
    Oppose,
    Neutral,
    Unknown,
}

#[derive(Debug, Serialize, Clone, Copy)]
pub enum RfAResult {
    Successful,
    Unsuccessful,
}

impl From<&str> for Position {
    fn from(value: &str) -> Self {
        match value {
            "Support" => Position::Support,
            "Oppose" => Position::Oppose,
            "Neutral" => Position::Neutral,
            _ => Position::Unknown,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct Vote {
    pub position: Position,
    pub result: RfAResult,
    pub page: String,
    pub short_name: String,
    pub content: Option<String>,
    pub is_rfb: bool,
}

impl Vote {
    pub fn new(
        position: Position,
        result: RfAResult,
        page: &Page,
        content: Option<String>,
    ) -> Self {
        Self {
            position,
            result,
            short_name: page.title().split_once('/').unwrap().1.to_string(),
            page: page.title().to_string(),
            is_rfb: page
                .title()
                .starts_with("Wikipedia:Requests for bureaucratship/"),
            // Yes, yes, this is evil.
            content: content.map(|val| {
                val.replace(
                    "href=\"./",
                    "href=\"https://en.wikipedia.org/wiki/",
                )
            }),
        }
    }
}
