// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

#[cfg(test)]
mod tests;

use anyhow::Result;
use mwbot::Bot;
use mysql_async::Pool;
use rfa_voting_history::{parser, query, Position, Vote};
use rocket::response::Debug;
use rocket::State;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use tokio::sync::OnceCell;

static BOT: OnceCell<Bot> = OnceCell::const_new();

async fn bot() -> &'static Bot {
    BOT.get_or_init(|| async {
        Bot::builder(
            "https://en.wikipedia.org/w/api.php".to_string(),
            "https://en.wikipedia.org/api/rest_v1".to_string(),
        )
        .set_user_agent(
            toolforge::user_agent!("rfa-voting-history").to_string(),
        )
        .build()
        .await
        .unwrap()
    })
    .await
}

async fn fetch_votes(
    bot: &Bot,
    pool: &Pool,
    username: &str,
    old: &Option<String>,
) -> Result<Vec<Vote>> {
    let pages = query::lookup(pool, username).await?;
    let mut tasks = vec![];
    let mut names = vec![username.to_string()];
    if let Some(old) = old {
        names.extend(old.split('|').map(|name| normalize_username(bot, name)));
    }
    for page in pages {
        let page = bot.page(&format!("Wikipedia:{}", page))?;
        let names = names.clone();
        tasks.push(tokio::spawn(async move {
            let names2: Vec<_> = names.iter().map(|x| x.as_str()).collect();
            parser::extract_vote(page, &names2).await
        }));
    }
    let mut votes = vec![];
    for result in tasks {
        let vote = result.await??;
        if let Some(vote) = vote {
            votes.push(vote);
        }
    }
    Ok(votes)
}

#[derive(Serialize)]
struct IndexTemplate {}

#[get("/")]
async fn index() -> Template {
    Template::render("index", IndexTemplate {})
}

#[derive(Serialize)]
struct VotesTemplate {
    username: String,
    old: Option<String>,
    votes: VoteMap,
}

#[derive(Default, Serialize)]
struct VoteMap {
    support: Vec<Vote>,
    oppose: Vec<Vote>,
    neutral: Vec<Vote>,
    unknown: Vec<Vote>,
}

impl VoteMap {
    fn add(&mut self, vote: Vote) {
        match vote.position {
            Position::Support => self.support.push(vote),
            Position::Oppose => self.oppose.push(vote),
            Position::Neutral => self.neutral.push(vote),
            Position::Unknown => self.unknown.push(vote),
        }
    }
}

fn normalize_username(bot: &Bot, username: &str) -> String {
    let userpage = bot.page(&format!("User:{}", username));
    match userpage {
        Ok(userpage) => {
            userpage.title().strip_prefix("User:").unwrap().to_string()
        }
        Err(_) => username.to_string(),
    }
}

#[get("/votes?<username>&<old>")]
async fn votes(
    username: String,
    old: Option<String>,
    pool: &State<Pool>,
) -> Result<Template, Debug<anyhow::Error>> {
    let bot = bot().await;
    let username = normalize_username(bot, &username);
    let mut votes = VoteMap::default();
    for vote in fetch_votes(bot, pool, &username, &old).await? {
        votes.add(vote);
    }

    Ok(Template::render(
        "votes",
        VotesTemplate {
            username,
            old,
            votes,
        },
    ))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(Pool::new(
            toolforge::connection_info!("enwiki", WEB)
                .expect("unable to load db config")
                .to_string()
                .as_str(),
        ))
        .mount("/", routes![index, votes])
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}
