// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
use crate::{Position, RfAResult, Vote};
use anyhow::Result;
use mwbot::parsoid::node::Wikinode;
use mwbot::parsoid::{Wikicode, WikinodeIterator};
use mwbot::Page;
use regex::Regex;
use std::sync::OnceLock;

const HEADINGS: [&str; 3] = ["Support", "Oppose", "Neutral"];
static SIGNATURE_RE: OnceLock<Regex> = OnceLock::new();

pub async fn extract_vote(page: Page, names: &[&str]) -> Result<Option<Vote>> {
    let html = page.html().await?;
    let code = html.into_mutable();
    let rfa_result = match determine_result(&code) {
        Some(result) => result,
        None => {
            println!("{} not an RfA", page.title());
            return Ok(None);
        }
    };
    let mut has_sections = false;
    for section in code.iter_sections() {
        let heading = match section.heading() {
            Some(heading) => heading,
            None => {
                continue;
            }
        };
        let heading_text = heading.text_contents();
        if !HEADINGS.contains(&heading_text.as_str()) {
            continue;
        }
        has_sections = true;
        if let Some(vote) = handle_section(
            section.select("ol > li"),
            names,
            &heading_text,
            &page,
            rfa_result,
        ) {
            return Ok(Some(vote));
        }
    }
    if has_sections {
        // Modern RfA with sections, but we didn't find a vote. Abort
        return Ok(Some(Vote::new(Position::Unknown, rfa_result, &page, None)));
    }
    // Old style RfA, need to find the <p><b>Support</b></p>, etc. markers
    // Slightly newer but still Old style RfAs have <dl><dt>Support</dt></dl>
    // because consistency.
    let mut headings = code.select("p > b");
    if !headings
        .iter()
        .any(|node| HEADINGS.contains(&node.text_contents().as_str()))
    {
        headings = code.select("dl dt");
    }
    for heading in headings {
        let heading_text = heading.text_contents();
        if !HEADINGS.contains(&heading_text.as_str()) {
            continue;
        }
        // Nice! Find the next list.
        for list in heading.parent().unwrap().following_siblings() {
            let items = list.select("ol > li");
            if items.is_empty() {
                continue;
            }
            if let Some(vote) =
                handle_section(items, names, &heading_text, &page, rfa_result)
            {
                return Ok(Some(vote));
            } else {
                break;
            }
        }
    }

    Ok(Some(Vote::new(Position::Unknown, rfa_result, &page, None)))
}

fn handle_section(
    items: Vec<Wikinode>,
    names: &[&str],
    heading_text: &str,
    page: &Page,
    rfa_result: RfAResult,
) -> Option<Vote> {
    // Iterate through each top-level comment (li)
    for item in items {
        // Detach all subcomments first
        for dd in item.select("dd") {
            dd.detach();
        }
        if !item.text_contents().contains("(UTC)") {
            // No signature? Skip!
            continue;
        }
        if guess_signature(&item, names) {
            return Some(Vote::new(
                Position::from(heading_text),
                rfa_result,
                page,
                Some(item.to_string()),
            ));
        }
    }
    None
}

fn guess_signature(item: &Wikinode, names: &[&str]) -> bool {
    let regex = SIGNATURE_RE.get_or_init(|| {
        Regex::new(
            r"^(User:|User talk:|Special:Contributions/|Special:Contribs/)(.*)$",
        ).unwrap()
    });
    // Run through it backwards since signatures are at the end of the comment
    // We stop on the first real username we find, but only look through the last
    // three links in the comment.
    for link in item.filter_links().into_iter().rev().take(3) {
        let target = link.target();
        if let Some(cap) = regex.captures(&target) {
            if cap[2].contains('/') {
                continue;
            }
            return names.contains(&&cap[2])
                || (cap[2].ends_with("#top")
                    && names.contains(&cap[2].strip_suffix("#top").unwrap()));
        }
    }

    false
}

fn determine_result(code: &Wikicode) -> Option<RfAResult> {
    for category in code.filter_categories() {
        let cat = category.category();
        if cat == "Category:Successful requests for adminship"
            || cat == "Category:Successful requests for bureaucratship"
        {
            return Some(RfAResult::Successful);
        } else if cat == "Category:Unsuccessful requests for adminship"
            || cat == "Category:Unsuccessful requests for bureaucratship"
        {
            return Some(RfAResult::Unsuccessful);
        }
    }

    // Not an RfA?
    None
}
