// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use mwseaql::{
    sea_query::{Expr, MysqlQueryBuilder, Order, Query},
    Actor, Categorylinks, Page, RevisionUserindex,
};
use mysql_async::prelude::*;
use mysql_async::Pool;

pub async fn lookup(pool: &Pool, name: &str) -> Result<Vec<String>> {
    // TODO: normalize username
    let query = Query::select()
        .distinct()
        .column(Page::Title)
        .from(RevisionUserindex::Table)
        .inner_join(
            Page::Table,
            Expr::col(RevisionUserindex::Page).equals(Page::Id),
        )
        .inner_join(
            Actor::Table,
            Expr::col(RevisionUserindex::Actor).equals(Actor::Id),
        )
        .inner_join(
            Categorylinks::Table,
            Expr::col(Page::Id).equals(Categorylinks::From),
        )
        .and_where(Expr::col(Page::Namespace).eq(4))
        .and_where(
            Expr::col(Page::Title)
                .like("Requests_for_adminship/%")
                .or(Expr::col(Page::Title)
                    .like("Requests_for_bureaucratship/%")),
        )
        .and_where(Expr::col(Categorylinks::To).is_in([
            "Successful_requests_for_adminship",
            "Successful_requests_for_bureaucratship",
            "Unsuccessful_requests_for_adminship",
            "Unsuccessful_requests_for_bureaucratship",
        ]))
        .and_where(Expr::col(Actor::Name).eq(name))
        .order_by(RevisionUserindex::Timestamp, Order::Desc)
        .to_string(MysqlQueryBuilder);
    println!("{}", &query);
    let mut conn = pool.get_conn().await?;
    let result: Vec<String> = conn.query(query).await?;
    println!("Got {} pages", &result.len());
    Ok(result)
}
